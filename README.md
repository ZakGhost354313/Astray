# Astray

A WebGL maze game built with Three.js and Box2dWeb. Play it here: http://www.3543.cf/Astray

### Launching

There are several ways to launch the game. Here is the simplest:

1. Clone or download the repository
2. Navigate to Astray's directory
3. Start 'python -m SimpleHTTPServer' in your shell (for python 3.0 and above type 'python -m http.server' in your shell)
4. Open 'localhost:8000' in your browser
5. Enjoy!

Another way if you have a github page:

1. fork repo
2. go to settings
3. go to pages settings
4. change the source to master/root
5. save
6. and then check that it works

### License

I don't believe in them. You can order your bits however you please.
